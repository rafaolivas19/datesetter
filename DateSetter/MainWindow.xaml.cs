﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DateSetter
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // show admin priviledges message
            if(!GotAdminRights())
            {
                AdminMessageTbk.Visibility = Visibility.Visible;
                Height += 80;
            }

            // set http security protocol
            ServicePointManager.ServerCertificateValidationCallback =
                (s, certificate, chain, sslPolicyErrors) => true;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // populate comboboxes
            for (int i = 0; i <= 24; i++)
            {
                HourCbx.Items.Add(i);
            }

            for (int i = 0; i <= 59; i++)
            {
                MinuteCbx.Items.Add(i);
                SecondCbx.Items.Add(i);
            }

            // set current date and hour
            var now = DateTime.Now;

            DateDtp.SelectedDate = now;
            HourCbx.SelectedItem = now.Hour;
            MinuteCbx.SelectedItem = now.Minute;
            SecondCbx.SelectedItem = now.Second;
        }

        private void CurrentBtn_Click(object sender, RoutedEventArgs e)
        {
            SetNistTime();
        }

        private static DateTime GetNistTime()
        {
            DateTime dateTime = DateTime.MinValue;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://nist.time.gov/actualtime.cgi?lzbc=siqm9b");
            request.Method = "GET";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
            request.ContentType = "application/x-www-form-urlencoded";
            request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore); //No caching
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                StreamReader stream = new StreamReader(response.GetResponseStream());
                string html = stream.ReadToEnd();//<timestamp time=\"1395772696469995\" delay=\"1395772696469995\"/>
                string time = Regex.Match(html, @"(?<=\btime="")[^""]*").Value;
                double milliseconds = Convert.ToInt64(time) / 1000.0;
                dateTime = new DateTime(1970, 1, 1).AddMilliseconds(milliseconds).ToLocalTime();
            }

            return dateTime;
        }

        [DllImport("kernel32.dll", EntryPoint = "GetSystemTime", SetLastError = true)]
        private extern static void GetSystemTime(ref SystemTime sysTime);

        private bool GotAdminRights()
        {
            using (WindowsIdentity identity = WindowsIdentity.GetCurrent())
            {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
        }

        private void SetBtn_Click(object sender, RoutedEventArgs e)
        {
            var date = DateDtp.SelectedDate;

            var newDate = new DateTime(
                date.Value.Year,
                date.Value.Month,
                date.Value.Day,
                int.Parse(HourCbx.Text),
                int.Parse(MinuteCbx.Text),
                int.Parse(SecondCbx.Text)
            ).ToUniversalTime();

            var sysDate = new SystemTime
            {
                Year = (ushort)newDate.Year,
                Month = (ushort)newDate.Month,
                Day = (ushort)newDate.Day,
                Hour = (ushort)newDate.Hour,
                Minute = (ushort)newDate.Minute,
                Second = (ushort)newDate.Second
            };

            SetSystemTime(ref sysDate);
        }

        private static void SetNistTime()
        {
            var date = GetNistTime().ToUniversalTime();

            var systemTime = new SystemTime
            {
                Year = (ushort)date.Year,
                Month = (ushort)date.Month,
                Day = (ushort)date.Day,
                Hour = (ushort)date.Hour,
                Minute = (ushort)date.Minute,
                Second = (ushort)date.Second
            };

            SetSystemTime(ref systemTime);
        }

        [DllImport("kernel32.dll", EntryPoint = "SetSystemTime", SetLastError = true)]
        private static extern bool SetSystemTime(ref SystemTime st);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SystemTime
    {
        public ushort Year;
        public ushort Month;
        public ushort DayOfWeek;
        public ushort Day;
        public ushort Hour;
        public ushort Minute;
        public ushort Second;
        public ushort Millisecond;
    };
}
