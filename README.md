Esta utilidad facilita el cambio rápido de la hora y fecha del sistema. También permite volver rápidamente a la hora y fecha actual.

## Instrucciones

![](https://trello-attachments.s3.amazonaws.com/5a7e0b5a0c5c79f9b196dd18/5c705e5e4cbb517eabbde060/6d4b062d3c047a0962064f6053dbee74/image.png)

1. El primer recuadro permite elegin una fecha ya sea escribiendola en el mismo campo de texto o eligiendola a través del selector.
1. Los siguiente 3 selectores representan la hora, minuto y segundo que se quiere configurar respectivamente.
1. El botón *Set* establecerá la hora y fecha seleccionados en los controles de arriba.
1. El botón *Current* establecerá la hora y fecha actuales en el sistema (valores obtenidos del sitio http://nist.time.gov/actualtime.cgi?lzbc=siqm9b).

## Notas importantes

1. La aplicación deberá ser iniciada con permisos de administrador, de otro modo no funcionará y mostrará un mensaje de alerta indicandolo.

    ![](https://trello-attachments.s3.amazonaws.com/5a7e0b5a0c5c79f9b196dd18/5c705e5e4cbb517eabbde060/575ade7fb0a44e967b4a2c608328d247/image.png)
    
1. Debe evitarse navegar en internet mediante un explorador mientras la fecha establecida no sea la actual ya que esto puede causar inconsistencias dependiendo del navegador.